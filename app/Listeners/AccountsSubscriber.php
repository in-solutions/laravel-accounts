<?php

namespace App\Listeners;

class AccountsSubscriber
{
    /**
     * Handle user login events.
     */
    public function onPaymentCreated($event) {
        // project specific actions when new entity is created
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Insolutions\Accounts\EventPaymentCreated',
            'App\Listeners\AccountsSubscriber@onPaymentCreated'
        );

    }

}