SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `t_account` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `t_account_operation` (
`id` bigint(20) unsigned NOT NULL,
  `account_id` int(10) unsigned NOT NULL,
  `change` decimal(20,4) NOT NULL,
  `currency` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `api_id` varchar(128) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `vs` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ks` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ss` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opositeAccountNo` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opositeBankCode` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opositeBankName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userIdentity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userMessage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `specification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `json_object` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `t_account`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `t_account_operation`
 ADD PRIMARY KEY (`id`), ADD KEY `account_id` (`account_id`);


ALTER TABLE `t_account`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_account_operation`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `t_account_operation`
ADD CONSTRAINT `t_account_operation_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `t_account` (`id`) ON UPDATE CASCADE;
