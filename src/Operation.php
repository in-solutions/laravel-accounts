<?php

namespace Insolutions\Accounts;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    //
    protected $table = 't_account_operation';

    protected $fillable = ['account_id','change','datetime','fio_id','json_object'];

    public function account() {
 		return $this->belongsTo('Insolutions\Accounts\Account', 'account_id');
    }
}
