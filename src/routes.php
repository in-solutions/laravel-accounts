<?php

Route::group(['prefix' => 'acc'], function () {
	
	Route::get('account', 'Insolutions\Accounts\AccountsController@getAccounts');

	Route::get('/account/{account_id}/operation', 'Insolutions\Accounts\AccountsController@operations');

});