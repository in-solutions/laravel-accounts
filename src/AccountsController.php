<?php

namespace Insolutions\Accounts;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;

class AccountsController extends Controller
{
    
	public function getAccounts(Request $r) {
		return response()->json(Account::paginate($r->perPage ?: 50));
	}

	public function operations() {
        return response()->json(Operation::orderBy('datetime','desc')->paginate());    
    }

}