<?php

namespace Insolutions\Accounts;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
    protected $table = 't_account';

    protected $fillable = ['name'];

    public function operations() {
        return $this->hasMany('Insolutions\Accounts\AccountOperation');
    }
}
